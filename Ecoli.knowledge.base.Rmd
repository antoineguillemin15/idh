---
title: "*E. coli* knowledge base"
author: "RB"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: html_document
editor_options: 
  chunk_output_type: inline
---


```{r setup, include=TRUE}
#knitr::opts_chunk$set(echo = TRUE)
options(width = 120)
# knitr::opts_chunk$set(cache=F)
# knitr::opts_chunk$set(fig.width=6, fig.height=6) # for rstudio
# knitr::opts_chunk$set(fig.width=14, fig.height=14) # for html
``` 

# Prérequis

Installation éventuelle des librairies R dans un environnement conda.

shell:
```{bash eval=F}
conda create -n idh
conda deactivate
conda activate idh
conda install r-base=4.0.2 r-reticulate r-tidyverse r-kableextra r-dt r-rmysql bioconductor-stringdb openjdk=11.0.8 r-knitr r-igraph scipy numpy openjdk
```

# Librairies et fonctions utilitaires

```{r chargement librairie}
library(reticulate) # to execute bash or python chunks (only in notebooks)
library(tidyverse)
library(magrittr)

library(kableExtra) # nicer tables
kabex = function(o) o %>% kable(format="html", escape=F) %>% kable_styling(bootstrap_options = c("striped"))
library(DT) # other nice tables
dted = function(o) o %>% datatable(rownames=F, filter="top", options=list(pageLength = 10), escape=F)

library(igraph)
igraph.options(vertex.color=NA)
igraph.options(vertex.label.cex=.6) # font size
igraph.options(vertex.label.family='sans')
igraph.options(vertex.size=2)
igraph.options(edge.label.cex=.6)
igraph.options(edge.label.family='sans')

library(STRINGdb) # client STRING
```


# Génome/Protéome

La première étape consiste à récupérer l'ensemble du génome/protéome qui nous intéresse. 

## STRINGdb Proteins

Création du répertoire pour les données de STRING (shell) :
```{bash eval=F}
mkdir string.data
```

Connexion au serveur de STRING (NCBI taxon id de Ecoli est 511145: https://string-db.org/cgi/input.pl?input_page_active_form=organism )
```{r echo=TRUE}
confidence_threshold = 700
stringdb1 = STRINGdb$new(version='11', species=511145, score_threshold=confidence_threshold, input_directory='string.data')
```

NCBI taxon id de Xanthomonas campestris pv campestris : 340
```{r}
stringdb2 = STRINGdb$new(version='11', species=340, score_threshold=confidence_threshold, input_directory='string.data')
```


Téléchargement du génome/protéome :
```{r}
proteins.ecoli = stringdb1$get_proteins() %>% as_tibble
proteins.ecoli %>% dted
```

```{r}
proteins.xcc = stringdb2$get_proteins() %>% as_tibble
proteins.xcc %>% dted
```


## Neo4j installation

Sites et documentations :

  * https://neo4j.com/
  * https://neo4j.com/developer/get-started/ 

Récupérer la dernière version : Téléchargement linux https://neo4j.com/download-center/#releases (onglet *Community server*)

Création des répertoires et désarchivage (shell):
```{bash eval=F}
mkdir -p neo4j/download
cd neo4j/download
version=4.1.3
tar xf neo4j-community-$version-unix.tar.gz
ln -s neo4j-community-$version neo4j-community
cd neo4j-community/
```

Démarrage et arrêt du serveur (shell):
```{bash eval=F}
./bin/neo4j console
```

Utilisation depuis le navigateur (vérifier le port renseigné lors de la précédente commande) : http://localhost:7474/

A la première connexion, le mot de passe est `neo4j`, le système demande ensuite de changer le mot de passe. 

Création d'un fichier CSV pour le protéome d'E. coli pour l'importer dans Neo4j (les sommets correspondent aux protéines) :

```{r}
proteins.ecoli %>%
  mutate(taxon_id = str_extract(protein_external_id, "([0-9]+)"), protein_id= str_match(protein_external_id, "\\.(.+)$")[,2]) %>%
  select(protein_external_id, taxon_id, protein_id, name=preferred_name, length=protein_size, annotation) %>%
  write_csv("neo4j/download/neo4j-community/import/ecoli.proteins.csv")
```

```{r}
proteins.xcc %>%
  mutate(taxon_id = str_extract(protein_external_id, "([0-9]+)"), protein_id= str_match(protein_external_id, "\\.(.+)$")[,2]) %>%
  select(protein_external_id, taxon_id, protein_id, name=preferred_name, length=protein_size, annotation) %>%
  write_csv("neo4j/download/neo4j-community/import/xcc.proteins.csv")
```


<!-- plus nécessaire depuis la 4.1.3
Pour pouvoir importer les données depuis un fichier local, il faut modifier la configuration de neo4j : dans le fichier `conf/neo4j.conf`, ajouter vers la ligne 227 :
```
dbms.security.allow_csv_import_from_file_urls=true
```
-->

Librairie neo4r
```{r}
if (!require('neo4r')) { # client neo4j (pas dispo avec conda)
  install.packages('neo4r')
}
library(neo4r)
neodb = neo4j_api$new(
  url = "http://localhost:7474", 
  user = "neo4j", 
  password = "bioinfo"
)
cypher = function(query, neo4j=neodb, ...) call_neo4j(query=query, con=neo4j, ...)
# 'MATCH (p:Protein {name:"dnaJ"}) RETURN p.protein_id, p.annotation' %>% cypher
```

Import dans neo4j

- pour E. coli :

```{r eval=F}
# DELETE PREVIOUS ATTEMPTS ON Protein
'MATCH ()-[r:STRINGdb]-() DELETE r' %>% cypher 
'MATCH ()-[r:GO_annotation]-() DELETE r' %>% cypher
'MATCH (n:Protein) DELETE n' %>% cypher
# IMPORT
'LOAD CSV WITH HEADERS FROM "file:///ecoli.proteins.csv" AS row 
CREATE (n:Protein)
SET n = row,
 n.protein_external_id = row.protein_external_id,
 n.taxon_id = toInteger(row.taxon_id),
 n.protein_id = row.protein_id,
 n.name = row.name,
 n.length = toInteger(row.length),
 n.annotation = row.annotation
'  %>% cypher
```

- pour Xcc :

```{r eval=F}
# IMPORT
'LOAD CSV WITH HEADERS FROM "file:///xcc.proteins.csv" AS row 
CREATE (n:Protein)
SET n = row,
 n.protein_external_id = row.protein_external_id,
 n.taxon_id = toInteger(row.taxon_id),
 n.protein_id = row.protein_id,
 n.name = row.name,
 n.length = toInteger(row.length),
 n.annotation = row.annotation
'  %>% cypher
```

Vérification *via* des requêtess cypher:
```{r}
'MATCH (p:Protein {name:"dnaJ"}) RETURN p.protein_id, p.annotation'  %>% cypher(type="row") 
'MATCH (p:Protein) WHERE p.length <= 40 RETURN p'  %>% cypher %>% extract2('p') %>% dted
'MATCH (p:Protein {taxon_id:511145}) RETURN count(p)'  %>% cypher
'MATCH (p:Protein {taxon_id:340}) RETURN count(p)'  %>% cypher 
```

Ajout d'un index (pour accélérer la création des liens par la suite):
```{r eval=F}
'CREATE INDEX ON :Protein(protein_external_id)' %>% cypher
```

# Données d'expression

Nous allons utiliser les données d'expression déjà intégrées dans STRINGdb. Il est bien sûr possible de calculer un score de co-expression entre les paires de gènes à partir de données de microarray et/ou RNAseq.

## STRINGdb detailed links → neo4j

Il faut passer par la page de téléchargement sur https://string-db.org/cgi/download.pl en restreignant l'organisme d'intérêt. 

Chargement sous forme de tibble :
```{r}
links.detailed.ecoli = read_delim("string.data/511145.protein.links.detailed.v11.0.txt.gz", delim=" ", col_types = "ccnnnnnnnn")
links.detailed.ecoli
```

```{r}
links.detailed.xcc = read_delim("string.data/340.protein.links.detailed.v11.0.txt.gz", delim=" ", col_types = "ccnnnnnnnn")
links.detailed.xcc
```

C'est la **colonne coexpression** qui nous intéresse.

Génération des fichiers CSV pour l'import :
```{r}
links.detailed.ecoli %>%
  filter(coexpression>0 & protein1 < protein2) %>%
  select(protein1, protein2, coexpression) %>%
  write_csv("neo4j/download/neo4j-community/import/ecoli.string.coexpression.csv")
```

```{r}
links.detailed.xcc %>%
  filter(coexpression>0 & protein1 < protein2) %>%
  select(protein1, protein2, coexpression) %>%
  write_csv("neo4j/download/neo4j-community/import/xcc.string.coexpression.csv")
```


Import dans neo4j :
```{r eval=F}
'
LOAD CSV WITH HEADERS FROM "file:///ecoli.string.coexpression.csv" AS line
MATCH (p1:Protein),(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.coexpression) AS value 
MERGE (p1)-[:STRINGdb {coexpression: value}]-(p2)
'  %>% cypher
```

```{r eval=F}
'
LOAD CSV WITH HEADERS FROM "file:///xcc.string.coexpression.csv" AS line
MATCH (p1:Protein),(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.coexpression) AS value 
MERGE (p1)-[:STRINGdb {coexpression: value}]-(p2)
'  %>% cypher
```

Vérification :
```{r}
'MATCH ({taxon_id:511145})-[r:STRINGdb]-({taxon_id:511145}) RETURN count(r)' %>% cypher
'MATCH ({taxon_id:340})-[r:STRINGdb]-({taxon_id:340}) RETURN count(r)' %>% cypher
```

Extraction des liens dont la probabilité est supérieure à 0.7 sous forme de tibble
```{r}
foo.ecoli = 'MATCH (p1 {taxon_id:511145})-[r:STRINGdb]-(p2 {taxon_id:511145}) WHERE r.coexpression>=700 RETURN p1.name, p2.name, r.coexpression' %>% cypher 
tibble(protein1=foo.ecoli$p1.name$value, protein2=foo.ecoli$p2.name$value, coexpression=foo.ecoli$r.coexpression$value) %>% dted
```

```{r}
foo.xcc = 'MATCH (p1 {taxon_id:340})-[r:STRINGdb]-(p2 {taxon_id:340}) WHERE r.coexpression>=700 RETURN p1.name, p2.name, r.coexpression' %>% cypher 
tibble(protein1=foo.xcc$p1.name$value, protein2=foo.xcc$p2.name$value, coexpression=foo.xcc$r.coexpression$value) %>% dted
```


Extraction d'un graphe
```{r}
g.coexpr.ecoli = 'MATCH p = ({taxon_id:511145})-[r:STRINGdb]-({taxon_id:511145}) WHERE r.coexpression>=700 RETURN p'  %>% cypher(type="graph")
```

```{r}
g.coexpr.xcc = 'MATCH p = ({taxon_id:340})-[r:STRINGdb]-({taxon_id:340}) WHERE r.coexpression>=700 RETURN p'  %>% cypher(type="graph")
```

Manipulation pour `igraph` pour récupérer les propriétés des sommets (https://github.com/neo4j-rstats/neo4r)
```{r}
g.coexpr.ecoli$nodes = g.coexpr.ecoli$nodes %>% unnest_nodes(what="properties")
g.coexpr.ecoli$nodes %>% dted
```

```{r}
g.coexpr.xcc$nodes = g.coexpr.xcc$nodes %>% unnest_nodes(what="properties")
g.coexpr.xcc$nodes %>% dted
```


Besoin de réordonner les liens
```{r}
g.coexpr.ecoli$relationships = g.coexpr.ecoli$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression))
g.coexpr.ecoli$relationships %>% dted
```

```{r}
g.coexpr.xcc$relationships = g.coexpr.xcc$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression))
g.coexpr.xcc$relationships %>% dted
```

Création dans igraph
```{r}
g.coexpr.ecoli = graph_from_data_frame(d=g.coexpr.ecoli$relationships, directed=F, vertices = g.coexpr.ecoli$nodes)
g.coexpr.ecoli
```

```{r}
g.coexpr.xcc = graph_from_data_frame(d=g.coexpr.xcc$relationships, directed=F, vertices = g.coexpr.xcc$nodes)
g.coexpr.xcc
```

Plot
```{r}
plot(g.coexpr.ecoli, vertex.label=NA, main='coexpression')
```

```{r}
plot(g.coexpr.xcc, vertex.label=NA, main='coexpression')
```

# Données d'interaction protéine-protéine
## STRINGdb protein-protein interactions → neo4j

Pour le réseau d'interaction ppi, la colonne `experimental` contient des interactions physiques et fonctionnelles. Un autre fichier est donc nécessaire : protein.actions (binding)

```{r}
actions.ecoli = read_delim("string.data/511145.protein.actions.v11.0.txt.gz", delim="\t")
actions.ecoli
```

```{r}
actions.xcc = read_delim("string.data/340.protein.actions.v11.0.txt.gz", delim="\t")
actions.xcc
```

Seulement les *binding*
```{r}
links.ppi.ecoli = actions.ecoli %>% 
  filter(mode=='binding' & item_id_a<item_id_b) %>% 
  dplyr::select(protein1 = item_id_a, protein2 = item_id_b, ppi = score) %>%
  filter(ppi >= confidence_threshold)
```

```{r}
links.ppi.xcc = actions.xcc %>% 
  filter(mode=='binding' & item_id_a<item_id_b) %>% 
  dplyr::select(protein1 = item_id_a, protein2 = item_id_b, ppi = score) %>%
  filter(ppi >= confidence_threshold)
```

`experimetal:` BIND, DIP, GRID, HPRD, IntAct, MINT, and PID. (src: https://string-db.org/help/faq/#which-databases-does-string-extract-experimental-data-from)

Comme précédemment, création du fichier CSV puis import dans neo4j
```{r eval=F}
links.ppi.ecoli %>% write_csv("neo4j/download/neo4j-community/import/ecoli.string.ppi.csv")
'LOAD CSV WITH HEADERS FROM "file:///ecoli.string.ppi.csv" AS line
MATCH (p1:Protein)-[r:STRINGdb]-(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.ppi) AS value
MERGE (p1)-[r:STRINGdb]-(p2)
SET r.ppi=value
'  %>% cypher
```

```{r eval=F}
links.ppi.xcc %>% write_csv("neo4j/download/neo4j-community/import/xcc.string.ppi.csv")
'LOAD CSV WITH HEADERS FROM "file:///xcc.string.ppi.csv" AS line
MATCH (p1:Protein)-[r:STRINGdb]-(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.ppi) AS value
MERGE (p1)-[r:STRINGdb]-(p2)
SET r.ppi=value
'  %>% cypher
```

```{r}
'MATCH p = (p1:Protein {name:"dnaJ"})-[r]-(p2:Protein {name:"dnaK"}) return p' %>% cypher
```

# Graphe de coexpression & ppi

Extraction du graphe et visualisation
```{r}
g.ppi.coexpr.ecoli = 'MATCH p = ({taxon_id:511145})-[r:STRINGdb]-({taxon_id:511145}) WHERE r.coexpression>=700 AND r.ppi>=700 RETURN p'  %>% cypher(type="graph")
g.ppi.coexpr.ecoli$nodes = g.ppi.coexpr.ecoli$nodes %>% unnest_nodes(what="properties")
g.ppi.coexpr.ecoli$relationships = g.ppi.coexpr.ecoli$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi))
g.ppi.coexpr.ecoli = graph_from_data_frame(d=g.ppi.coexpr.ecoli$relationships, directed=F, vertices = g.ppi.coexpr.ecoli$nodes)
g.ppi.coexpr.ecoli
plot(g.ppi.coexpr.ecoli, vertex.label=NA, main='Ecoli coexpr & ppi')
```

```{r}
g.ppi.coexpr.xcc = 'MATCH p = ({taxon_id:340})-[r:STRINGdb]-({taxon_id:340}) WHERE r.coexpression>=700 AND r.ppi>=700 RETURN p'  %>% cypher(type="graph")
g.ppi.coexpr.xcc$nodes = g.ppi.coexpr.xcc$nodes %>% unnest_nodes(what="properties")
g.ppi.coexpr.xcc$relationships = g.ppi.coexpr.xcc$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi))
g.ppi.coexpr.xcc = graph_from_data_frame(d=g.ppi.coexpr.xcc$relationships, directed=F, vertices = g.ppi.coexpr.xcc$nodes)
g.ppi.coexpr.xcc
plot(g.ppi.coexpr.xcc, vertex.label=NA, main='Xcc coexpr & ppi')
```

Les contraintes de coexpression > 0.7 et de ppi > 0.7 semblent un peu trop restrictives. Nous allons prendre un OR à la place pour avoir un graphe plus dense :
```{r}
g.ppi.coexpr.ecoli = 'MATCH p = ({taxon_id:511145})-[r:STRINGdb]-({taxon_id:511145}) WHERE r.coexpression>=700 OR r.ppi>=700 RETURN p'  %>% cypher(type="graph")
g.ppi.coexpr.ecoli$nodes = g.ppi.coexpr.ecoli$nodes %>% unnest_nodes(what="properties")
g.ppi.coexpr.ecoli$relationships = g.ppi.coexpr.ecoli$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi))
g.ppi.coexpr.ecoli = graph_from_data_frame(d=g.ppi.coexpr.ecoli$relationships, directed=F, vertices = g.ppi.coexpr.ecoli$nodes)
g.ppi.coexpr.ecoli
plot(g.ppi.coexpr.ecoli, vertex.label=NA, vertex.size=1, main="Ecoli coexpr | ppi")
```

```{r}
g.ppi.coexpr.xcc = 'MATCH p = ({taxon_id:340})-[r:STRINGdb]-({taxon_id:340}) WHERE r.coexpression>=700 OR r.ppi>=700 RETURN p'  %>% cypher(type="graph")
g.ppi.coexpr.xcc$nodes = g.ppi.coexpr.xcc$nodes %>% unnest_nodes(what="properties")
g.ppi.coexpr.xcc$relationships = g.ppi.coexpr.xcc$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi))
g.ppi.coexpr.xcc = graph_from_data_frame(d=g.ppi.coexpr.xcc$relationships, directed=F, vertices = g.ppi.coexpr.xcc$nodes)
g.ppi.coexpr.xcc
plot(g.ppi.coexpr.xcc, vertex.label=NA, vertex.size=1, main="Xcc coexpr | ppi")
```


# Données phylogénomiques

Dans STRINGdb, ces données sont disponibles pour les bactéries. Elles résultent de l'analyse de la conservation des gènes orthologues dans le voisinage sur les chromosomes d'autres organismes.

## Ajout des liens *conserved genomic neighborhood*

Génération des fichiers CSV pour l'import comme pour *coexpression*
```{r echo=F}
links.detailed.ecoli %>%
  filter(neighborhood>0 & protein1 < protein2) %>%
  select(protein1, protein2, neighborhood) %>%
  write_csv("neo4j/download/neo4j-community/import/ecoli.string.neighborhood.csv")
```

```{r echo=F}
links.detailed.xcc %>%
  filter(neighborhood>0 & protein1 < protein2) %>%
  select(protein1, protein2, neighborhood) %>%
  write_csv("neo4j/download/neo4j-community/import/xcc.string.neighborhood.csv")
```

Ajout des liens dans neo4j
```{r eval=F}
'LOAD CSV WITH HEADERS FROM "file:///ecoli.string.neighborhood.csv" AS line
MATCH (p1:Protein)-[r:STRINGdb]-(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.neighborhood) AS value
MERGE (p1)-[r:STRINGdb]-(p2)
SET r.neighborhood=value
'  %>% cypher
```

```{r eval=F}
'LOAD CSV WITH HEADERS FROM "file:///xcc.string.neighborhood.csv" AS line
MATCH (p1:Protein)-[r:STRINGdb]-(p2:Protein)
WHERE p1.protein_external_id=line.protein1 AND p2.protein_external_id=line.protein2
WITH p1,p2, toInteger(line.neighborhood) AS value
MERGE (p1)-[r:STRINGdb]-(p2)
SET r.neighborhood=value
'  %>% cypher
```

Extraction du graphe et visualisation
```{r}
g.string.ecoli = 'MATCH p = ({taxon_id:511145})-[r:STRINGdb]-({taxon_id:511145}) WHERE r.coexpression>=700 OR r.ppi>=700 OR r.neighborhood>=700 RETURN p'  %>% cypher(type="graph")
g.string.ecoli$nodes = g.string.ecoli$nodes %>% unnest_nodes(what="properties")
g.string.ecoli$relationships = g.string.ecoli$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi), neighborhood = unlist(neighborhood))
g.string.ecoli = graph_from_data_frame(d=g.string.ecoli$relationships, directed=F, vertices = g.string.ecoli$nodes)
g.string.ecoli
plot(g.string.ecoli, vertex.label=NA, vertex.size=1, main='Ecoli coexpr | pppi | neighborhood')
```

```{r}
g.string.xcc = 'MATCH p = ({taxon_id:340})-[r:STRINGdb]-({taxon_id:340}) WHERE r.coexpression>=700 OR r.ppi>=700 OR r.neighborhood>=700 RETURN p'  %>% cypher(type="graph")
g.string.xcc$nodes = g.string.xcc$nodes %>% unnest_nodes(what="properties")
g.string.xcc$relationships = g.string.xcc$relationships %>% 
  unnest_relationships %>% 
  select(startNode, endNode, type, everything()) %>% 
  mutate(coexpression = unlist(coexpression), ppi = unlist(ppi), neighborhood = unlist(neighborhood))
g.string.xcc = graph_from_data_frame(d=g.string.xcc$relationships, directed=F, vertices = g.string.xcc$nodes)
g.string.xcc
plot(g.string.xcc, vertex.label=NA, vertex.size=1, main='Xcc coexpr | pppi | neighborhood')
```

Tailles des composantes connexes
```{r}
clusters(g.string.ecoli)$csize
```

```{r}
clusters(g.string.xcc)$csize
```

Extraction des sous-graphes correspondant aux CC de taille minimum 10
```{r}
CCs.num.ecoli = which(clusters(g.string.ecoli)$csize >=10)
CCs.size.ecoli = clusters(g.string.ecoli)$csize[CCs.num.ecoli]
V.ids.ecoli = V(g.string.ecoli)[ clusters(g.string.ecoli)$membership %in% CCs.num.ecoli ]
g.ecoli = induced_subgraph(g.string.ecoli, V.ids.ecoli)
plot(g.ecoli, vertex.label=NA)
```

```{r}
CCs.num.xcc = which(clusters(g.string.xcc)$csize >=10)
CCs.size.xcc = clusters(g.string.xcc)$csize[CCs.num.xcc]
V.ids.xcc = V(g.string.xcc)[ clusters(g.string.xcc)$membership %in% CCs.num.xcc ]
g.xcc = induced_subgraph(g.string.xcc, V.ids.xcc)
plot(g.xcc, vertex.label=NA)
```

Calcul du score combiné avant détection de communautés (source: http://version11.string-db.org/help/faq/#how-are-the-scores-computed)
```{r}
prior=0.041
no_prior = function(x, prior = 0.041) (ifelse(is.na(x), 0, x) / 1000 - prior) / (1-prior)
s_coexp_nop.ecoli = no_prior(E(g.ecoli)$coexpression)
s_ppi_nop.ecoli = no_prior(E(g.ecoli)$ppi)
s_neighborhood_nop.ecoli = no_prior(E(g.ecoli)$neighborhood)
s_tot_nop.ecoli = 1 - (1 - s_coexp_nop.ecoli) * (1 - s_ppi_nop.ecoli) * (1 - s_neighborhood_nop.ecoli)
E(g.ecoli)$combined_score = round(1000 * (s_tot_nop.ecoli + prior *(1 - s_tot_nop.ecoli)))
```

```{r}
s_coexp_nop.xcc = no_prior(E(g.xcc)$coexpression)
s_ppi_nop.xcc = no_prior(E(g.xcc)$ppi)
s_neighborhood_nop.xcc = no_prior(E(g.xcc)$neighborhood)
s_tot_nop.xcc = 1 - (1 - s_coexp_nop.xcc) * (1 - s_ppi_nop.xcc) * (1 - s_neighborhood_nop.xcc)
E(g.xcc)$combined_score = round(1000 * (s_tot_nop.xcc + prior *(1 - s_tot_nop.xcc)))
```


Détection de communautés prenant en compte le score combiné
```{r}
g.communities.ecoli = cluster_louvain(g.ecoli, weights=E(g.ecoli)$combined_score)
plot(g.communities.ecoli, g.ecoli, vertex.label=NA)
```

```{r}
g.communities.xcc = cluster_louvain(g.xcc, weights=E(g.xcc)$combined_score)
plot(g.communities.xcc, g.xcc, vertex.label=NA)
```

Nombre de communautés
```{r}
length(g.communities.ecoli)
```

```{r}
length(g.communities.xcc)
```


Tailles des communautés
```{r}
sizes(g.communities.ecoli)
```

```{r}
sizes(g.communities.xcc)
```


Modularité
```{r}
modularity(g.ecoli, g.communities.ecoli$membership, weights=E(g.ecoli)$combined)
```

```{r}
modularity(g.xcc, g.communities.xcc$membership, weights=E(g.xcc)$combined)
```


# Génération des fichiers par clusters/communautés pour la recherche d'enrichissement en termes GO
```{r}
tibble(protein_id = V(g.ecoli)$protein_id, name=V(g.ecoli)$name, cluster = g.communities.ecoli$membership) %>% dted
```

```{r}
tibble(protein_id = V(g.xcc)$protein_id, name=V(g.xcc)$name, cluster = g.communities.xcc$membership) %>% dted
```

```{r}
groups(g.communities.ecoli) %>% head(2)
```

```{r}
groups(g.communities.xcc) %>% head(2)
```

Remplacement des noms par les identifiants "stringdb"
```{r}
g.clusters.ecoli = lapply(groups(g.communities.ecoli), function(x) V(g.ecoli)[ name %in% x]$protein_id)
head(g.clusters.ecoli, 2)
```

```{r}
g.clusters.xcc = lapply(groups(g.communities.xcc), function(x) V(g.xcc)[ name %in% x]$protein_id)
head(g.clusters.xcc, 2)
```

Les fichiers seront générés dans les dossiers créés ci-dessous (shell) :

```{bash}
mkdir enrichment.ecoli
```

```{bash}
mkdir enrichment.xcc
```


Génération des fichiers
```{r}
lines.ecoli = sapply(g.clusters.ecoli, function(x) paste(x, collapse = '\t')) %>% simplify2array 
write_lines(lines.ecoli, "enrichment.coli/g.communities.ecoli.txt")
```

```{r}
lines.xcc = sapply(g.clusters.xcc, function(x) paste(x, collapse = '\t')) %>% simplify2array 
write_lines(lines.xcc, "enrichment.xcc/g.communities.xcc.txt")
```

Split by lines (shell)
```{bash eval=F}
split -l 1 --numeric-suffix=1 --additional-suffix=.txt enrichment.coli/g.communities.ecoli.txt g.com.ecoli.
mv g.com.ecoli.*.txt enrichment.coli/
```

```{bash eval=F}
split -l 1 --numeric-suffix=1 --additional-suffix=.txt enrichment.xcc/g.communities.xcc.txt g.com.xcc.
mv g.com.xcc.*.txt enrichment.xcc/
```

# Données d'annotation
## Gene Ontology → neo4j


## Restauration de la base fournie par GO

* télécharger le dump sur go.org http://archive.geneontology.org/latest-termdb/go_daily-termdb-data.gz
* restaurer la base
* extraire le contenu -> go_daily-termdb-tables

Librairie MariaDb/MySQL
```{r}
library(RMySQL) # client MariaDb
dbh=dbConnect(MySQL(), user="root", password="bioinfo", dbname="go_idh_2020", host="127.0.0.1")
dbget = function(query) as_tibble(dbGetQuery(dbh, query))
```


Variables d'environnement (shell) qui facilitent les changements de nom de DB *etc.* (shell)
```{bash eval=F}
dbname=go_idh_2020
mysql="mysql -uroot -pbioinfo $dbname"
mysql -uguest -pbioinfo -e "CREATE DATABASE $dbname"
```

Restauration de GO db (shell)
```{bash eval=F}
mkdir go.data
cd go.data
wget http://archive.geneontology.org/latest-termdb/go_daily-termdb-data.gz
gunzip go_daily-termdb-data.gz
$mysql < go_daily-termdb-data
```

## Extraction de `go.terms.csv` pour import dans neo4j
```{r}
"SELECT id, acc, term_type, name FROM term 
WHERE acc LIKE 'GO:%' AND is_obsolete=0 
LIMIT 10;
" %>% dbget %>% head %>% kabex
```

Export dans un shell
```{bash eval=F}
$mysql -e "SELECT id, acc, term_type, name FROM term WHERE acc LIKE 'GO:%' AND is_obsolete=0;" > neo4j/download/neo4j-community/import/go.terms.tsv
```

## Ajout des GOTerm dans Neo4j

* put data into neo4j subdir "import" qui correspond dans neoj browser à file://
* import des termes GO dans Neo4j 

```{r eval=F}
"MATCH ()-[r:is_a]->() DELETE r" %>% cypher
"MATCH ()-[r:part_of]->() DELETE r" %>% cypher
"MATCH (n:GOTerm) DELETE n" %>% cypher
"
LOAD CSV WITH HEADERS FROM 'file:///go.terms.tsv' AS row FIELDTERMINATOR '\t'
CREATE (n:GOTerm)
SET n = row,
  n.id = toInteger(row.id),
  n.acc = row.acc,
  n.term_type = row.term_type,
  n.name = row.name  
" %>% cypher
```

```{r}
"MATCH (n:GOTerm) RETURN count(n)" %>% cypher
```

Création des index pour accélérer la suite
```{r eval=F}
'CREATE INDEX ON :GOTerm(id)' %>% cypher

'CREATE INDEX ON :GOTerm(acc)' %>% cypher
```

Exploration
```{r}
"MATCH (n:GOTerm { name: 'reproduction'} ) RETURN n" %>% cypher
```

## Ajout des relations entre GOTerms

Il faut identifier les types is_a (1) et part_of (20) dans la table term
```{r}
"SELECT * FROM term WHERE name = 'is_a' OR name = 'part_of';" %>% dbget %>% kabex
```

```{bash eval=F}
is_a=1
part_of=20
$mysql -B -e "SELECT term1_id, term2_id FROM term2term WHERE relationship_type_id = $is_a;" > neo4j/download/neo4j-community/import/go.rel.is_a.dump.tsv
```

```{r eval=F}
"MATCH ()-[r:is_a]->() DELETE r" %>% cypher
"
LOAD CSV WITH HEADERS FROM 'file:///go.rel.is_a.dump.tsv' AS line FIELDTERMINATOR '\t' 
MATCH (t1:GOTerm),(t2:GOTerm) 
WHERE t1.id=toInteger(line.term1_id) AND t2.id=toInteger(line.term2_id) 
WITH t1,t2 
MERGE (t2)-[:is_a]->(t1)
" %>% cypher
```


```{r}
"MATCH ()-[r:is_a]->() RETURN count(r)" %>% cypher
```

```{bash eval=F}
$mysql -B -e "SELECT term1_id, term2_id FROM term2term WHERE relationship_type_id = $part_of;" > neo4j/download/neo4j-community/import/go.rel.part_of.dump.tsv
```

```{r eval=F}
"MATCH ()-[r:part_of]->() DELETE r" %>% cypher
"
LOAD CSV WITH HEADERS FROM 'file:///go.rel.part_of.dump.tsv' AS line FIELDTERMINATOR '\t'
MATCH (t1:GOTerm),(t2:GOTerm)
WHERE t1.id=toInteger(line.term1_id) AND t2.id=toInteger(line.term2_id)
WITH t1,t2
MERGE (t2)-[:part_of]->(t1)
" %>% cypher
```


```{r}
"MATCH ()-[r:part_of]->() RETURN count(r)" %>% cypher
```

## Ajout de l'annotation d'un génome/protéome


### GO Annotations hébergées à l'EBI

Fichier GOA de l'EBI : ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/ 

Celui d'Ecoli 
```{bash eval=F}
mkdir go.data
curl ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/18.E_coli_MG1655.goa -o go.data/18.E_coli_MG1655.goa
curl ftp://ftp.ebi.ac.uk/pub/databases/GO/goa/proteomes/87.X_campestris_campestris.goa -o go.data/87.X_campestris_campestris.goa
```

```{bash}
head go.data/18.E_coli_MG1655.goa
head go.data/87.X_campestris_campestris.goa
```

Le fichier est au format GAF2.1

Chargement (noms des colonnes de http://geneontology.org/docs/go-annotation-file-gaf-format-2.1/)
```{r}
gaf.cnames = c('DB', 'DB Object ID','DB Object Symbol', 'Qualifier', 'GO ID', 'DB:Reference', 'Evidence Code', 'With (or) From', 'Aspect', 'DB Object Name', 'DB Object Synonym', 'DB Object Type', 'Taxon', 'Date', 'Assigned By', 'Annotation Extension', 'Gene Product Form ID')

go.ecoli = read_tsv('go.data/18.E_coli_MG1655.goa', skip = 2, col_names = gaf.cnames)
go.ecoli %>% head %>% kabex
```

```{r}
go.xcc = read_tsv('go.data/87.X_campestris_campestris.goa', skip = 2, col_names = gaf.cnames)
go.xcc %>% head %>% kabex
```


Les identifiants utilisés dans le graphe de STRING sont sur la colonne `DB Object Synonym`. mais nécessitent d'être parsés :
```{r}
go.ecoli %>% select(toparse=`DB Object Synonym`) %>% mutate(bnumber=str_extract(toparse, 'b\\d+'))
```

```{r}
go.xcc = go.xcc %>% mutate(`DB Object Synonym`=tolower(`DB Object Synonym`))
go.xcc %>% select(toparse=`DB Object Synonym`) %>% mutate(xccnumber=str_extract(toparse, 'xcc\\d+'))
```

Ok, on garde seulement les colonnes qui nous intéressent
```{r}
go.ecoli = go.ecoli %>% 
  mutate(bnumber=str_extract(`DB Object Synonym`, 'b\\d+')) %>%
  mutate(protein_external_id=paste0('511145.',bnumber)) %>%
  select(GOTerm=`GO ID`, evidence_code=`Evidence Code`,protein_external_id)
go.ecoli
```

```{r}
go.xcc = go.xcc %>% 
  mutate(xccnumber=str_extract(`DB Object Synonym`, 'xcc\\d+')) %>%
  mutate(protein_external_id=paste0('340.',xccnumber)) %>%
  select(GOTerm=`GO ID`, evidence_code=`Evidence Code`,protein_external_id)
go.xcc
```

Génération des fichiers pour l'import dans neo4j
```{r}
go.ecoli %>% write_csv("neo4j/download/neo4j-community/import/ecoli.goannotation.csv")
```

```{r}
go.xcc %>% write_csv("neo4j/download/neo4j-community/import/xcc.goannotation.csv")
```

Ajout des liens dans neo4j
```{r eval=F}
'MATCH ()-[r:GO_annotation]-() DELETE r' %>% cypher
'
LOAD CSV WITH HEADERS FROM "file:///ecoli.goannotation.csv" AS line
MATCH (p:Protein),(t:GOTerm)
WHERE p.protein_external_id=line.protein_external_id AND t.acc=line.GOTerm
WITH p,t,line
MERGE (p)-[r:GO_annotation]-(t)
SET r.evidence_code=line.evidence_code
'  %>% cypher
```

```{r eval=F}
'
LOAD CSV WITH HEADERS FROM "file:///xcc.goannotation.csv" AS line
MATCH (p:Protein),(t:GOTerm)
WHERE p.protein_external_id=line.protein_external_id AND t.acc=line.GOTerm
WITH p,t,line
MERGE (p)-[r:GO_annotation]-(t)
SET r.evidence_code=line.evidence_code
'  %>% cypher
```

```{r}
'MATCH ()-[r:GO_annotation]-() RETURN count(r)' %>% cypher
```

## Exploration

Annotations directes
```{r}
"
MATCH (:Protein {name:'dnaJ'})-[:GO_annotation]-(t:GOTerm) RETURN t ORDER BY t.name
" %>% cypher
```

Annotations implicites
```{r}
"
MATCH (:Protein {name:'dnaJ'})-[:GO_annotation]-(:GOTerm)-[*]->(t:GOTerm) 
RETURN DISTINCT t 
ORDER BY t.name
" %>% cypher  %>% 
  extract2('t') %>% 
  select(-id) %>%
  dted
```

Annotations directes et implicites
```{r}
terms = "
MATCH (:Protein {name:'dnaJ'})-[:GO_annotation]-(t_direct:GOTerm)-[*]->(t_implicit:GOTerm) 
RETURN t_direct, t_implicit 
" %>% cypher
terms$t_direct %>% unique %>% arrange(name) %>% select(-id) %>% kabex
```

```{r}
terms$t_implicit %>% unique %>% arrange(name) %>% select(-id) %>% dted
```

Protéines annotées directement par un terme
```{r}
"
MATCH (p:Protein)-[:GO_annotation]-(:GOTerm {name: 'chaperone binding'}) 
RETURN DISTINCT p 
" %>% cypher %>% extract2('p') %>% select(-annotation) %>% kabex
```


Terme *chaperone binding* trop spécifique, prenons-en un plus générique
```{r}
"
MATCH (p:Protein)-[:GO_annotation]-(:GOTerm)-[:is_a|part_of*]->(:GOTerm {name: 'protein folding'}) 
RETURN DISTINCT p
ORDER BY p.name
" %>% cypher %>% extract2('p') %>% select(-annotation) %>% dted
```


Protéines annotées directement ou implicitement
```{r}
"
MATCH (p:Protein)-[:GO_annotation|is_a|part_of*]->(:GOTerm {name: 'protein folding'}) 
RETURN DISTINCT p
ORDER BY p.name
" %>% cypher %>% extract2('p') %>% select(-annotation) %>% dted
```

# Génération des fichiers pour la recherche de termes GO sur-représentés

Fonction pour récupérer les termes directs
```{r}
get_GeneProducts = function(goterm, all=F) {
  query = paste0("MATCH (p:Protein)-[:GO_annotation]-(:GOTerm {acc: '", goterm, "'}) RETURN DISTINCT p.protein_id ORDER BY p.protein_id")
  if (all) {
  query = paste0("MATCH (p:Protein)-[:GO_annotation|is_a|part_of*]->(:GOTerm {acc: '", goterm,"'}) RETURN DISTINCT p.protein_id ORDER BY p.protein_id")
  }
  res = query %>% cypher 
  if (length(res) > 0) {
    res = res %>% extract2(1) %>% unlist
  }
  else {
    res = c()
  }
  res
}
```


```{r}
get_GeneProducts("GO:0006457", all=F)
```


```{r}
get_GeneProducts("GO:0006457", all=T)
```

Identification des termes GO concernant *Ecoli*
```{r}
#go.ecoli %>% select(GOTerm) %>% unique %>% arrange(GOTerm)
ecoli.terms = "
MATCH (:Protein {taxon_id:511145})-[:GO_annotation|is_a|part_of*]->(t:GOTerm) 
RETURN DISTINCT t.acc
" %>% cypher %>% unlist
```

```{r}
xcc.terms = "
MATCH (:Protein {taxon_id:340})-[:GO_annotation|is_a|part_of*]->(t:GOTerm) 
RETURN DISTINCT t.acc
" %>% cypher %>% unlist

```


Direct GeneProducts
```{r warning=F, message=F, eval=F}
go.sets.direct = lapply(ecoli.terms, get_GeneProducts)
names(go.sets.direct)=ecoli.terms
go.sets.direct[['GO:0006457']]
```

Fichiers *sets*
```{r, eval=F}
header = paste0("# format: sets
# version: 1.3
# strain: Escherichia coli K-12 MG1655
# date: ", format(Sys.time(), '%d %B, %Y'), "
# comment: Gene Ontology terms")
sets = lapply(ecoli.terms, function(x) paste(go.sets.direct[[x]], collapse = '|')) %>% unlist
tb = tibble(GOTerm=ecoli.terms, GeneProducts = sets)
go.terms = read_tsv("neo4j/download/neo4j-community/import/go.terms.tsv")
lines = tb %>% inner_join(go.terms, by=c('GOTerm'='acc')) %>% mutate(desc=paste0(term_type,': ', name)) %>% mutate(txt=paste(GOTerm, desc, GeneProducts, sep='\t')) %>% filter(str_detect(GeneProducts,"\\|"))
write_lines(header,"enrichment.coli/ecoli.go.direct.sets")
write_lines(lines$txt,"enrichment.coli/ecoli.go.direct.sets", append=T)
```



Direct and implicit GeneProducts
```{r warning=F, message=F, eval=F}
go.sets.implicit = lapply(ecoli.terms, function(x) get_GeneProducts(x, all=T))
names(go.sets.implicit)=ecoli.terms
go.sets.implicit[['GO:0006457']]
```

Fichiers *sets*
```{r, eval=F}
header = paste0("# format: sets
# version: 1.3
# strain: Escherichia coli K-12 MG1655
# date: ", format(Sys.time(), '%d %B, %Y'), "
# comment: Gene Ontology terms")
sets = lapply(ecoli.terms, function(x) paste(go.sets.implicit[[x]], collapse = '|')) %>% unlist
tb = tibble(GOTerm=ecoli.terms, GeneProducts = sets)
lines = tb %>% inner_join(go.terms, by=c('GOTerm'='acc')) %>% mutate(desc=paste0(term_type,': ', name)) %>% mutate(txt=paste(GOTerm, desc, GeneProducts, sep='\t')) %>% filter(str_detect(GeneProducts,"\\|"))
write_lines(header,"enrichment.coli/ecoli.go.implicit.sets")
write_lines(lines$txt,"enrichment.coli/ecoli.go.implicit.sets", append=T)
```


# Gene Set Enrichment Analysis

```{bash eval=F}
git clone git@gitlab.com:rbarriot/enrichment.git
```

```{bash}
for i in $(seq 1 9); do 
  ./enrichment/blastset.py -q enrichment.coli/g.com.0$i.txt -t enrichment.coli/ecoli.go.direct.sets -c > enrichment.coli/g.com.0$i.enriched.tsv
done
for i in $(seq 10 42); do 
  ./enrichment/blastset.py -q enrichment.coli/g.com.$i.txt -t enrichment.coli/ecoli.go.direct.sets -c > enrichment.coli/g.com.$i.enriched.tsv
done
```

Résultats
```{r}
read_tsv("enrichment.coli/g.com.01.enriched.tsv") %>% kabex
```


```{r warning=F, message=F,  results='asis'}
cat(sapply(2:9, function(i) kabex(read_tsv(paste0("enrichment.coli/g.com.0",i,".enriched.tsv")))))
cat(sapply(10:42, function(i) kabex(read_tsv(paste0("enrichment.coli/g.com.",i,".enriched.tsv")))))
```


# DELETE ALL nodes & edges
```{r eval=F}
MATCH ()-[r]-() DELETE r; # DELETE ALL RELATIONSHIPS
MATCH (n) DELETE n;       # DELETE ALL NODES
```

